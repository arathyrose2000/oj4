#include <bits/stdc++.h>
#define size 100009
char str[size];
int seg[size * 4]; //segtree is 1 indexing
int char_to_int(char c) { return (int)(c - '0'); }
int left_child(int pos) { return 2 * pos + 1; }
int right_child(int pos) { return 2 * pos + 2; }
void build_tree(int pos, int low, int high) //if 1 indexing initial parameters (1, 1, size)
{
    if (low == high) //if leaf
        seg[pos] = char_to_int(str[low]);
    else //if internal node
    {
        int mid = (low + high) / 2;
        build_tree(left_child(pos), low, mid);
        build_tree(right_child(pos), mid + 1, high);
        seg[pos] = seg[left_child(pos)] + seg[right_child(pos)];
    }
}
int query(int l, int r, int pos, int low, int high) //start with l,r,1,1,n
{
    if (high < l || low > r)        //no overlap
        return 0;                   //junk
    else if (l <= low && high <= r) //complete overlap
        return seg[pos];
    else //partial overlap
        return (query(l, r, left_child(pos), low, (high + low) / 2) + query(l, r, right_child(pos), (high + low) / 2 + 1, high));
}
void update(int l, int r, int pos, int index_updated, int new_value) //l->low, r->high 1 n 1
{
    if (l == r)
        seg[pos] = str[index_updated] = new_value;
    else
    {
        if (index_updated <= (l + r) / 2)
            update(l, (l + r) / 2, left_child(pos), index_updated, new_value);
        else
            update((l + r) / 2, r, right_child(pos), index_updated, new_value);
        seg[pos] = (seg[left_child(pos)] + seg[right_child(pos)]); //rebuild
    }
}
int main()
{
    int n, q, in, y, l, r;
    char garbage;
    scanf("%d %d", &n, &q);
    scanf("%s", str); //make a segtree
    scanf("%c", garbage);
    int type;
    while (q--)
    {
        scanf("%d", type);
        if (type == 1)
        {
            scanf("%d %d", &in, &y);
            update(1, n, 1, in, y);
        }
        else if (type == 2)
        {
            scanf("%d %d", &l, &r);
            query(l, r, 1, 1, n);
        }
    }
}