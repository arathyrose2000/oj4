#include <stdio.h>
long long int c;
int chTin(char s)
{
    return ((int)s - 48);
}
long long int sum[1000009], count[1000009], pre[10][1000009], suf[10][1000009], N, Q, k; /// segtree index from 1 for ease of getting parent
char str[100009];
void makeSegTree(long long int l, long long int r, long long int i)
{
    if (l == r)
    {
        int x = chTin(str[l]);
        sum[i] = x;
        for (k = 0; k < 10; k++)
        {
            pre[k][i] = 0;
            suf[k][i] = 0;
        }
        count[i] = 0;
        if (x % 9 == 0)
            count[i] = 1;
        pre[x % 9][i] = 1;
        suf[x % 9][i] = 1;
        return;
    }
    long long int m = (l + r) / 2;
    makeSegTree(l, m, 2 * i);
    makeSegTree(m + 1, r, 2 * i + 1);
    sum[i] = sum[2 * i] + sum[2 * i + 1];
    count[i] = count[2 * i] + count[2 * i + 1];
    for (k = 0; k < 9; k++)
    {
        count[i] += suf[k][2 * i] * pre[(9 - k) % 9][2 * i + 1];
        pre[k][i] += pre[k][2 * i];
        pre[(sum[2 * i] + k) % 9][i] += pre[k][2 * i + 1];
        suf[k][i] += suf[k][2 * i + 1];
        suf[(sum[2 * i + 1] + k) % 9][i] += suf[k][2 * i];
    }
    return;
}
void change(long long int l, long long int r,
            long long int digit, long long int ind, long long int i)
{
    for (k = 0; k < 10; k++)
    {
        pre[k][i] = 0;
        suf[k][i] = 0;
    }
    if (l == r)
    {
        int x = digit;
        sum[i] = x;
        for (k = 0; k < 10; k++)
        {
            pre[k][i] = 0;
            suf[k][i] = 0;
        }
        count[i] = 0;
        if (x % 9 == 0)
            count[i] = 1;
        pre[x % 9][i] = 1;
        suf[x % 9][i] = 1;
        return;
    }
    long long int m = (l + r) / 2;
    if (ind <= m)
        change(l, m, digit, ind, 2 * i);
    else
        change(m + 1, r, digit, ind, 2 * i + 1);
    sum[i] = sum[2 * i] + sum[2 * i + 1];
    count[i] = count[2 * i] + count[2 * i + 1];
    for (k = 0; k < 9; k++)
    {
        count[i] += suf[k][2 * i] * pre[(9 - k) % 9][2 * i + 1];
        pre[k][i] += pre[k][2 * i];
        pre[(sum[2 * i] + k) % 9][i] += pre[k][2 * i + 1];
        suf[k][i] += suf[k][2 * i + 1];
        suf[(sum[2 * i + 1] + k) % 9][i] += suf[k][2 * i];
    }
    return;
}
long long int Counter(long long int l, long long int r, long long int lg, long long int rg, long long int i)
{
    c++;
    long long int j = c;
    for (k = 0; k < 10; k++)
    {
        pre[k][j] = 0;
        suf[k][j] = 0;
    }
    count[j] = 0;
    sum[j] = 0;
    if (lg <= l && rg >= r)
    {
        printf("pos:%lld;;;   ", i);
        return i;
    }
    if (lg > r || rg < l){ printf("c:%lld---", j);
        return j;}
    long long int m = (l + r) / 2;
    long long int c1, c2;
    c1 = Counter(l, m, lg, rg, 2 * i);
    c2 = Counter(m + 1, r, lg, rg, 2 * i + 1);
    printf("#%lld %lld %lld", j, c1, c2);
    sum[j] = sum[c1] + sum[c2];
    count[j] = count[c1] + count[c2];
    for (k = 0; k < 9; k++)
    {
        count[j] += suf[k][c1] * pre[(9 - k) % 9][c2];
        pre[k][j] += pre[k][c1];
        pre[(sum[c1] + k) % 9][j] += pre[k][c2];
        suf[k][j] += suf[k][c2];
        suf[(sum[c2] + k) % 9][j] += suf[k][c1];
    }
    printf(" %lld ", count[j]);
    return j;
} // make c = 300000 everytime in query of type 2;
int main(void)
{
    scanf("%lld %lld", &N, &Q);
    scanf("%s", str);
    makeSegTree(0, N - 1, 1);
    for (int i = 0; i < 2 * N + 2; i++)
    {
        printf("\nSUM=%lld\tPREFIX=", sum[i]);
        for (int I = 0; I < 9; I++)
            printf("%lld", pre[I][i]);
        printf("\tSUFFIX=");
        for (int I = 0; I < 9; I++)
            printf("%lld", suf[I][i]);
        printf("\tCOUNT=%lld", count[i]);
    }
    printf("\n");
    int as;
    // for(as = 1; as < 2*N; as++)
    // printf("%lld ", count[as]);
    int chd;
    long long int L, R, ans;
    while (Q--)
    {
        scanf("%d %lld %lld", &chd, &L, &R);
        if (chd == 1)
            change(0, N - 1, R, L - 1, 1);
        else
        {
            c = 300000;
            ans = Counter(0, N - 1, L - 1, R - 1, 1);
            printf("%lld\n", ans);
            long long int B = (R - L + 1) * (R - L + 2) / 2;
            long long int P = 1;
            long long int MOD = 1000000007;
            long long int EXP = MOD - 2;
            B = B % MOD;
            while (EXP > 0)
            {
                if (EXP % 2 == 1)
                {
                    P = (P * B) % MOD;
                }
                B = (B * B) % MOD;
                EXP /= 2;
            }
            printf("\n%lld, %lld, ", count[ans], P);
            printf("%lld\n", (count[ans] * P) % MOD);
        }
    }
    return 0;
}