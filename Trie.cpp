#include <stdio.h>
#include <iostream>
#include <stdlib.h>
using namespace std;
// #define struct node struct struct node
#define d 1
struct node
{
    struct node *children[5];
    int isEndofWord; //if 1 then it is end of word
};
struct node *newnode()
{
    struct node *n = (struct node *)malloc(sizeof(struct node));
    n->isEndofWord = 0;
    n->children[0] = n->children[1] = n->children[2] = n->children[3] = n->children[4] = NULL;
    return n;
}
struct node *root;
void insert(char s[])
{
    struct node *p = root;
    for (int i = 0; s[i] != '\0'; i++)
    {
        if (!p->children[s[i] - 'a'])
            p->children[s[i] - 'a'] = newnode();
        p = p->children[s[i] - 'a'];
    }
    p->isEndofWord = 1;
}
int search(char s[], struct node *p, int cur, char c)
{
    int ans = 0;
    if (d)
        cout << ":CHECK " << c << ":\t";
    if (s[cur] == '\0' && p != NULL && p->isEndofWord == 1)
    {
        if (d)
            cout << "END OF WORD\n";
        return 1;
    }
    if (p == NULL)
        return 0;
    if (c == '\0')
        return 0;
    if (c != '?')
    {
        if (p->children[c - 'a'])
        {
            if (d)
                cout << c << " ";
            return search(s, p->children[c - 'a'], cur + 1, s[cur + 1]);
        }
    }
    else
    {
        if (d)
            cout << "\n?:   ";
        if (cur==0||cur > 0 && s[cur - 1] != '?')
            ans = search(s, p, cur + 1, s[cur + 1]);
        ans += search(s, p, cur, 'a') + search(s, p, cur, 'b') + search(s, p, cur, 'c') + search(s, p, cur, 'd') + search(s, p, cur, 'e');
    }
    return ans;
}
void display(struct node *n, char str[], int level)
{
    if (n->isEndofWord)
    {
        str[level] = '\0';
        cout << str << endl;
    }
    for (int i = 0; i < 5; i++)
    {
        if (n->children[i])
        {
            str[level] = i + 'a';
            display(n->children[i], str, level + 1);
        }
    }
}
int main()
{
    int n;
    cin >> n;
    char str[100000];
    root = newnode();
    for (int i = 0; i < n; i++)
    {
        cin >> str;
        insert(str);
    }
    if (d)
    {
        printf("\n");
        display(root, str, 0);
    }
    int q;
    cin >> q;
    for (int i = 0; i < q; i++)
    {
        cin >> str;
        cout << search(str, root, 0, str[0]) << "\n";
    }
    return 0;
}