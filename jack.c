#include <stdio.h>
#define max 100009
#define int long long int
#define d 0
int a[max], given[max], c[max], queue_stack[max];
int top = -1, bottom = -1;
void insert_top(int a)
{
    if (top == -1)
        top = bottom = 0;
    else if (top == 0)
        top = max - 1;
    else
        top--;
    queue_stack[top] = a;
}
void insert_bottom(int a) //insert at bottom bottom++
{
    if (top == -1) //if empty
        top = bottom = 0;
    else if (bottom == max - 1)
        bottom = 0;
    else
        bottom++;
    queue_stack[bottom] = a;
}
void delete_top() //delete from top top++
{
    if (top == bottom)
        top = bottom = -1;
    else if (top == max - 1)
        top = 0;
    else
        top++;
}
void delete_bottom() // bottom --
{
    if (top == bottom)
        top = bottom = -1;
    else if (bottom == 0)
        bottom = max - 1;
    else
        bottom--;
}
int peek_top()
{
    return queue_stack[top];
}
int peek_bottom()
{
    return queue_stack[bottom];
}
int minsum(int n, int k)
{
    int sum = 0;
    top = -1;
    if (n == 1)
        return given[0];
    else if (k >= n - 1)
        return given[0] + given[n - 1];
    else
    {
        for (int i = 0; i < n; i++)
        {
            a[i] = ((top == -1) ? given[i] : a[peek_bottom()] + given[i]);
            while (!(top == -1) && peek_bottom() < i - k + 1)
                delete_bottom();
            while (!(top == -1) && a[i] <= a[peek_top()])
                delete_top();
            insert_top(i);
        }
        return a[n - 1];
    }
}
signed main()
{
    int n, t;
    scanf("%lld %lld", &n, &t);
    for (int i = 1; i < n; i++)
        scanf("%lld", &c[i]);
    given[0] = given[n - 1] = 0;
    for (int i = 1; i < n - 1; i++)
        scanf("%lld", &given[i]);
    if (d)
    {
        printf("n=%lld, t=%lld\n", n, t);
        printf("The time to get in and out are: \n");
        for (int i = 0; i < n; i++)
            printf("%3lld : %3lld \n", i, given[i]);
        printf("The minimum time per station are:\n");
        for (int i = 1; i < n; i++)
            printf("%3lld : %3lld : %3lld\n", i, minsum(n, i) + n - 1, c[i]);
    }
    int first = 1, last = n - 1, tempk = n - 1;
    while (first <= last)
    {
        if (d)
            printf("first=%lld, last=%lld\n", first, last);
        int mid = (first + last) / 2;
        int time = minsum(n, mid) + n - 1;
        if (time <= t)
        {
            last = mid - 1;
            tempk = mid;
        }
        else
            first = mid + 1;
    }
    if (d)
        printf("Cutoff at: %lld, i.e. all values of c greater than %lld can be the answer\n", tempk, tempk);
    int mini = c[tempk];
    for (int i = tempk; i < n; i++)
        if (c[mini] > c[i])
            mini = i;
    printf("%lld\n", c[mini]);
}