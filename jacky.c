#include <bits/stdc++.h>
#define int long long int
#define size 1000009
int dp[size], given[size], stack[size], cost[size];
int bottom, top;
signed main(void)
{
    int n, T;
    scanf("%lld %lld", &n, &T);
    for (int i = 1; i < n; i++)
        scanf("%lld", &cost[i]);
    T = T - n + 1;
    given[0] = given[n - 1] = 0;
    for (int i = 1; i < n - 1; i++)
        scanf("%lld", &given[i]);
    int answer = 10000000, low = 1, high = n, pos = 10000000;
    while (low <= high)
    {
        int mid = (low + high) / 2;
        top = bottom = -1;
        for (int i = 0; i < n; i++)
        {
            dp[i] = (top == -1) ? given[i] : (dp[stack[bottom]] + given[i]);
            while (!(top == -1) && (stack[bottom] < i - mid + 1))
            {
                int is_single = (top == bottom);
                top = is_single ? -1 : top;
                bottom = is_single ? -1 : ((bottom + size - 1) % size);
            }
            while (!(top == -1) && (dp[i] <= dp[stack[top]]))
            {
                int is_single = (top == bottom);
                top = is_single ? -1 : ((top + 1) % size);
                bottom = is_single ? -1 : bottom;
            }
            bottom = (top == -1) ? 0 : bottom;
            top = (top == -1) ? 0 : ((top + size - 1) % size);
            stack[top] = a;
        }
        if (dp[n - 1] > T)
            low = mid + 1;
        else
        {
            high = mid - 1;
            pos = (mid < pos) ? mid : pos;
        }
    }
    for (int i = pos; i < n; i++)
        answer = (cost[i] < answer) ? cost[i] : answer;
    printf("%lld\n", answer);
    return 0;
}