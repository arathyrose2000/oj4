#include <bits/stdc++.h>
#define integer long long int
#define size 1000009
#define d 0
integer dp[size], given[size], stack[size], cost[size];
integer bottom, top;
int isempty()
{
    return (top == -1);
}
void push_bottom(int a)
{
    bottom = isempty() ? 0 : bottom;
    top = isempty() ? 0 : ((top + size - 1) % size);
    stack[top] = a;
}
void pop_bottom()
{
    int is_single = (top == bottom);
    top = is_single ? -1 : top;
    bottom = is_single ? -1 : ((bottom + size - 1) % size);
}
void pop_top()
{
    int is_single = (top == bottom);
    top = is_single ? -1 : ((top + 1) % size);
    bottom = is_single ? -1 : bottom;
}
signed main(void)
{
    integer n, T;
    scanf("%lld %lld", &n, &T);
    T = T - n + 1;
    for (integer i = 1; i < n; i++)
        scanf("%lld", &cost[i]);
    given[0] = given[n - 1] = 0;
    for (integer i = 1; i < n - 1; i++)
        scanf("%lld", &given[i]);
    integer answer = 10000000, low = 1, high = n, pos = 10000000;
    if (d)
    {
        printf("n=%lld, top=%lld\n", n, T);
        printf("The time to get in and out are: \n");
        for (integer i = 0; i < n; i++)
            printf("\ttop%3lld : %3lld \n", i, given[i]);
        printf("The minimum cost:\n");
        for (integer i = 1; i < n; i++)
            printf("\ttop%3lld : %3lld \n", i, cost[i]);
    }
    while (low <= high)
    {
        integer mid = (low + high) / 2;
        top = bottom = -1;
        for (integer i = 0; i < n; i++)
        {
            dp[i] = isempty() ? given[i] : (dp[stack[bottom]] + given[i]);
            while (!isempty() && (stack[bottom] < i - mid + 1))
                pop_bottom();
            while (!isempty() && (dp[i] <= dp[stack[top]]))
                pop_top();
            push_bottom(i);
        }
        if (d)
            printf("\nnFirst: %4lld     Last: %4lld    Mid: %4lld    Time: %4lld", low, high, mid, dp[n - 1]);
        if (dp[n - 1] > T)
            low = mid + 1;
        else
        {
            high = mid - 1;
            pos = (mid < pos) ? mid : pos;
        }
    }
    for (integer i = pos; i < n; i++)
        answer = (cost[i] < answer) ? cost[i] : answer;
    printf("%lld\n", answer);
    return 0;
}