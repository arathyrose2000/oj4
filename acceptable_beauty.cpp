#include <bits/stdc++.h>
#define debug 0
#define integer int
#define getkey() scanf("%s", key)
integer noid = 0, sum = 0;
char key[2009];
integer find_integer(char c)
{
    return (integer)(c - 'a');
}
class node
{
  public:
    node *children[5];
    integer end, idk;
    node *newnode()
    {
        node *new_node = (node *)malloc(sizeof(node));
        for (integer i = 0; i < 5; i++)
            new_node->children[i] = NULL;
        new_node->end = new_node->idk = 0;
        return new_node;
    }
    void insert()
    {
        getkey();
        node *temp = this;
        for (integer i = 0; i < strlen(key); i++)
        {
            if (!temp->children[find_integer(key[i])])
                temp->children[find_integer(key[i])] = newnode();
            temp = temp->children[find_integer(key[i])];
        }
        temp->end++;
    }
    void display(char str[], integer level)
    {
        if (end)
        {
            str[level] = '\0';
            printf("%s\n", str);
        }
        for (integer i = 0; i < 5; i++)
        {
            if (children[i])
            {
                str[level] = i + 'a';
                children[i]->display(str, level + 1);
            }
        }
    }
};
node *root;
integer check_index(char key[], integer index)
{
    if (index > strlen(key))
        return 1;
    if (index < strlen(key))
        return -1;
    return 0;
}
void search_key(char key[], node *current, integer index)
{
    if (!current || check_index(key, index) == 1)
    {
        if (debug)
            printf("\nCASE 1: invalid");
    }
    else if (check_index(key, index) == 0 && current->end > 0)
    {
        if (debug)
            printf("\nCASE 2:");
        if (current->idk <= noid)
        {
            if (debug)
                printf("string found");
            current->idk = noid + 1;
            sum += current->end;
        }
        else if (debug)
            printf("String already read");
    }
    else if (key[index] == '?' && check_index(key, index) == -1)
    {
        if (debug)
            printf("\nCASE 3: ??");
        for (integer i = 0; i < 5; i++)
            if (current->children[i])
                search_key(key, current->children[i], index + 1);
        if (debug)
            printf("here");
        search_key(key, current, index + 1);
    }
    else if (current->children[find_integer(key[index])])
    {
        if (debug)
            printf("\nCASE 4: normal");
        search_key(key, current->children[find_integer(key[index])], index + 1);
    }
}
signed main()
{
    node *root = root->newnode();
    integer n, q;
    scanf("%d", &n);
    while (n--)
        root->insert();
    if (debug)
    {
        printf("\n\nThe strings read are:\n");
        root->display(key, 0);
    }
    scanf("%d", &q);
    while (q--)
    {
        getkey();
        sum = 0;
        search_key(key, root, 0);
        printf("%d\n", sum);
        noid++;
    }
}