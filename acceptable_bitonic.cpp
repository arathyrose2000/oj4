#include <bits/stdc++.h>
using namespace std;
#define maxsize 1000009
#define int long long int
#define MOD 1000000007
#define d 0
int segCount[maxsize * 3];
int N;
struct duo
{
    int index, value;
} arr[maxsize], temp[maxsize];
struct segTree_node
{
    int a0;
    int a1;
    int a2;
    int a3;
} segArr[maxsize * 3];
int left(int index) { return 2 * index; }
int right(int index) { return 2 * index + 1; }
void merge(int low, int high)
{
    int mid = (low + high) / 2;
    int l1 = low, i = low, l2 = mid + 1;
    while (l1 <= mid && l2 <= high)
        temp[i++] = (arr[l1].value <= arr[l2].value) ? arr[l1++] : arr[l2++];
    while (l1 <= mid)
        temp[i++] = arr[l1++];
    while (l2 <= high)
        temp[i++] = arr[l2++];
    for (int i = low; i <= high; i++)
        arr[i] = temp[i];
}
void sort(int low, int high)
{
    if (low < high)
    {
        sort(low, (low + high) / 2);
        sort((low + high) / 2 + 1, high);
        merge(low, high);
    }
}
void buildSum(int low, int high, int pos, int index, int val)
{
    segCount[pos] = (segCount[pos] + val) % MOD;
    if (low < high)
        (index <= (low + high) / 2) ? buildSum(low, (low + high) / 2, left(pos), index, val) : buildSum((low + high) / 2 + 1, high, right(pos), index, val);
}
int querySum(int low, int high, int pos, int l, int r)
{
    return ((l > high || r < low) ? 0 : ((l <= low && high <= r) ? segCount[pos] % MOD : ((querySum(low, (low + high) / 2, left(pos), l, r) + querySum((low + high) / 2 + 1, high, right(pos), l, r)) % MOD)));
}
void init_segCount()
{
    for (int i = 0; i < maxsize * 3; i++)
        segCount[i] = 0;
}
signed main()
{
    cin >> N;
    arr[0].value = arr[0].index = 0;
    for (int i = 1; i <= N; i++)
        cin >> segArr[i].a0;
    for (int i = 1; i <= N; i++)
    {
        arr[i].index = i;
        arr[i].value = segArr[i].a0;
    }
    if (d)
    {
        cout << "\nThe given array before sorting is:  \t";
        for (int i = 1; i <= N; i++)
            cout << arr[i].value << "(" << arr[i].index << ")  , ";
    }
    sort(1, N); //1 indexing of the segarray
    if (d)
    {
        cout << "\nThe given array after sorting is:  \t";
        for (int i = 1; i <= N; i++)
            cout << arr[i].value << "(" << arr[i].index << ")  , ";
    }
    int k = 0;
    for (int i = 1; i <= N; i++)
    {
        k = k + (arr[i - 1].value < arr[i].value);
        segArr[arr[i].index].a0 = k;
    }
    if (d)
        cout << "\n\nINCREASING PART";
    init_segCount();
    for (int i = 1; i <= N; i++)
    {
        segArr[i].a3 = 1 + querySum(1, N, 1, 0, segArr[i].a0);
        buildSum(1, N, 1, segArr[i].a0, segArr[i].a3);
    }
    if (d)
    {
        cout << "\nThe segCount array now is: ";
        for (int j = 1; j <= 2 * N + 3; j++)
            cout << segCount[j] << ",";
        cout << "\nThe segArr(a3) now contains ";
        for (int j = 1; j <= 2 * N + 3; j++)
            cout << segArr[j].a3 << ",";cout << "\nThe segArr(a0) now contains ";
        for (int j = 1; j <= 2 * N + 3; j++)
            cout << segArr[j].a0 << ",";
    }
    if (d)
        cout << "\n\nDECREASING PART";
    init_segCount();
    for (int i = N; i > 0; i--)
    {
        segArr[i].a1 = 1 + querySum(1, N, 1, 0, segArr[i].a0 - 1);
        segArr[i].a2 = 1 + querySum(1, N, 1, 0, segArr[i].a0);
        buildSum(1, N, 1, segArr[i].a0, segArr[i].a2);
    }
    if (d)
    {
        cout << "\nThe segCount array now is: ";
        for (int j = 1; j <= 2 * N + 3; j++)
            cout << segCount[j] << ",";
        cout << "\nThe segArr(a1) now contains ";
        for (int j = 1; j <= 2 * N + 3; j++)
            cout << segArr[j].a1 << ",";
        cout << "\nThe segArr(a2) now contains ";
        for (int j = 1; j <= 2 * N + 3; j++)
            cout << segArr[j].a2 << ",";cout << "\nThe segArr(a0) now contains ";
        for (int j = 1; j <= 2 * N + 3; j++)
            cout << segArr[j].a0 << ",";
    }
    int ans = 0;
    for (int i = 1; i <= N; i++)
    {
        if (d)
            cout << "\nAnswer now: " << ans << "\tSegArr values: " << segArr[i].a3 << ", " << segArr[i].a1;
        ans = (ans + ((segArr[i].a3) % MOD * (segArr[i].a1) % MOD) % MOD) % MOD;
    }
    if (d)
        cout << "\nFinal Answer is: ";
    cout << ans;
}