#include<stdio.h>
long long int eterarr[1000009],answer,i, arr[1000009],stack[1000009], money[1000009];
long long int n,k,sum=0,t=-1,b=-1,T;
#define d 1

int call1()
{
    while ((t >= 0) && stack[b] < i - k + 1)
    {
        if (t == b)
        {
            b = -1;
            t = -1;
        }
        else
        {
            b = (b + 1000009 - 1) % 1000009;
        }    
    }
    return 0;
}
int call2()
{
    while((t >= 0) && eterarr[i] <= eterarr[stack[t]])
    {
        if (t == b)
        {
            b = -1;
            t = -1;
        }
        else
        {
            t++;
            t %= 1000009;
        }
    }
    return 0;
}
long long int ETERSUN()
{
    for (i = 0; i < n; i++)
    {
        if(t == -1) eterarr[i] = arr[i]; 
        else eterarr[i] =  eterarr[stack[b]] + arr[i];
        call1();
        call2();    
        if (t != -1)
        {
                t = (t + 1000009 - 1) % 1000009;
        }
        else
        {
            b = 0;
            t = 0;
        }
        stack[t] = i;
    }
    return eterarr[n-1];
}
long long int findminR()
{
    long long int l = 1, r = n, m, R = 10000000;
    k = (l+r)/2; 
    while(l <= r)
    {
        sum=0;t=-1;b=-1;
        if (d)
            printf("\nFirst: %4lld     Last: %4lld    Mid: %4lld    Time: %4lld", l, r, k, ETERSUN());
        sum=0;t=-1;b=-1;
        if(ETERSUN() > T) // cant reach on time 
        {
            l = k + 1;
        }
        else
        {
            if(k < R)       R = k;
            r = k -1;
        }
        k = (l + r)/2;
    }
    return R;
}
int main(void)
{
    scanf("%lld %lld", &n, &T);
    T -= n-1;
    for(i = 1 ; i < n; i++)
        scanf("%lld", &money[i]);
    for(i = 1; i < n-1; i++)
        scanf("%lld", &arr[i]);
    arr[0] = 0;
    arr[n - 1] = 0;
    if (d)
    {
        printf("n=%lld, t=%lld\n", n, T);
        printf("The time to get in and out are: \n");
        for (int i = 0; i < n; i++)
            printf("\t%3lld : %3lld \n", i, arr[i]);
        printf("The minimum cost:\n");
        for (int i = 1; i < n; i++)
            printf("\t%3lld : %3lld \n", i, money[i]);
    }
    long long int pos = findminR(), answ = 10000000;
    for(i = pos; i < n; i++) if(money[i] < answ) answ = money[i];   
    printf("%lld\n", answ);
    return 0;
}