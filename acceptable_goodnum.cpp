#include <bits/stdc++.h>
using namespace std;
#define int long long int
#define maxsize 1000009
#define d 0
#define MOD 1000000007
class node
{
  public:
    int sum; //sum of digits till now
    int prefix[10];
    int suffix[10];
    int count;
    void initialise(int x)
    {
        sum = x;
        count = (x % 9 == 0);
        for (int k = 0; k < 10; k++)
            prefix[k] = suffix[k] = 0;
        prefix[x % 9] = suffix[x % 9] = 1;
    }
    void display()
    {
        cout << "\nSUM=" << sum << "\tPREFIX=";
        for (int i = 0; i < 9; i++)
            cout << prefix[i];
        cout << "\tSUFFIX=";
        for (int i = 0; i < 9; i++)
            cout << suffix[i];
        cout << "\tCOUNT=" << count;
    }
};
int C;
int char_to_int(char s) { return (int)(s - '0'); }
node SegTree[maxsize]; //zero indexed
int left(int in) { return 2 * in; }
int right(int in) { return 2 * in + 1; }
char str[maxsize];
void operation(int pos, int left, int right)
{
    SegTree[pos].sum = SegTree[left].sum + SegTree[right].sum;
    SegTree[pos].count = SegTree[left].count + SegTree[right].count;
    for (int k = 0; k < 9; k++)
    {
        SegTree[pos].count += SegTree[left].suffix[k] * SegTree[right].prefix[(9 - k) % 9];
        SegTree[pos].prefix[k] += SegTree[left].prefix[k];
        SegTree[pos].prefix[(SegTree[left].sum + k) % 9] += SegTree[right].prefix[k];
        SegTree[pos].suffix[k] += SegTree[right].suffix[k];
        SegTree[pos].suffix[(SegTree[right].sum + k) % 9] += SegTree[left].suffix[k];
    }
}
void buildTree(int low, int high, int pos)
{
    if (low == high)
        SegTree[pos].initialise(char_to_int(str[low]));
    else
    {
        buildTree(low, (low + high) / 2, left(pos));
        buildTree((low + high) / 2 + 1, high, right(pos));
        operation(pos, left(pos), right(pos));
    }
}
void update(int low, int high, int pos, int new_digit, int ind)
{
    for (int i = 0; i < 10; i++)
        SegTree[pos].prefix[i] = SegTree[pos].suffix[i] = 0;
    if (low == high)
        SegTree[pos].initialise(new_digit);
    else
    {
        int m = (low + high) / 2;
        if (ind <= m)
            update(low, m, left(pos), new_digit, ind);
        else
            update(m + 1, high, right(pos), new_digit, ind);
        operation(pos, left(pos), right(pos));
    }
}
int query(int low, int high, int pos, int l, int r)
{
    C++;
    int c = C;
    SegTree[c].initialise(0);
    SegTree[c].prefix[0] = SegTree[c].suffix[0] = 0;
    SegTree[c].count = SegTree[c].sum = 0;
    if (l <= low && r >= high)
    {
        if (d)
            cout << "pos:" << pos << ";;;   ";
        return pos;
    }
    if (l > high || r < low)
    {
        if (d)
            cout << "c:" << c << "---";
        return c;
    }
    int m = (low + high) / 2;
    int c1 = query(low, m, left(pos), l, r);
    int c2 = query(m + 1, high, right(pos), l, r);
    if (d)
        cout << "#" << c << " " << c1 << " " << c2;
    operation(c, c1, c2);
    if (d)
        cout << " " << SegTree[c].count << " ";
    return c;
}
int modInverse(int a, int m)
{
    //return power(a,m-2,m);
    int y = m - 2, ans = 1;
    while (y > 0)
    {
        if (y % 2 == 1)
            ans = (ans * a) % m;
        y = y / 2;
        a = (a * a) % m;
    }
    return ans;
}
signed main()
{
    int N, Q;
    cin >> N >> Q >> str;
    buildTree(0, N - 1, 1);
    if (d)
    {
        for (int i = 0; i < 2 * N + 2; i++)
            SegTree[i].display();
        cout << "\n";
    }
    int type, index, new_digit, left_index, right_index, L, R, ans;
    while (Q--)
    {
        cin >> type;
        if (type == 1)
        {
            cin >> index >> new_digit;
            update(0, N - 1, 1, new_digit, index - 1);
        }
        else
        {
            C = 300000;
            cin >> L >> R;
            ans = query(0, N - 1, 1, L - 1, R - 1);
            int B = ((R - L + 1) * (R - L + 2) / 2) % MOD;
            if (d)
                cout << ans << "\n"
                     << SegTree[ans].count << ", " << modInverse(B, MOD) << ", ";
            cout << (SegTree[ans].count * modInverse(B, MOD)) % MOD << "\n";
        }
    }
    return 0;
}